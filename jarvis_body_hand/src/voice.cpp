#include "jarvis_body_hand/voice.hpp"

namespace jarvis_body_hand {

Voice::Voice(ros::NodeHandle& nodeHandle)
    : nodeHandle_(nodeHandle)
{
	pubTTS = nodeHandle_.advertise<std_msgs::String> 
									 ("/text_to_speech", 10, false);
}

Voice::~Voice()
{
}


void Voice::Talk(std::string talkStr)
{
	std_msgs::String talk;
    talk.data = talkStr;
    pubTTS.publish(talk);
    pubTTS.publish(talk);
}



} /* namespace */
